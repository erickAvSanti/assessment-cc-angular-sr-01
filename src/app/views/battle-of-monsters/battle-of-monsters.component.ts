import { Component, OnInit } from '@angular/core';
import { Monster } from '../../interfaces/monster.interface';
import { MonstersService } from '../../services/monsters.service';

@Component({
  selector: 'app-battle-of-monsters',
  templateUrl: './battle-of-monsters.component.html',
  styleUrls: ['./battle-of-monsters.component.scss'],
})
export class BattleOfMonstersComponent implements OnInit {
  public player!: Monster | null;
  public computer!: Monster | null;
  public monsters: Monster[] = [];

  battleResult: any = null;

  constructor(private monsterService: MonstersService) {}

  ngOnInit(): void {
    this.monsterService.getAll().subscribe((res) => {
      this.monsters = res;
    });
  }

  monsterSelected(monster: Monster | null) {
    this.player = monster;
    this.computer = null;
    if (this.player == null) return;
    const filteredMonsters = this.monsters.filter(
      (monster) => monster.id !== this.player?.id
    );
    const computerMonsterIndex = Math.floor(
      Math.random() * filteredMonsters.length
    ); // 0.9 * 9 = 8.1  8
    this.computer = filteredMonsters[computerMonsterIndex];
  }

  battle() {
    if(this.player == null) return;
    if(this.computer == null) return;
    this.monsterService
      .battle(this.player.id, this.computer.id)
      .subscribe((result) => this.battleResult = result);
  }
}
