import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonsterBattleCardPropComponent } from './monster-battle-card-prop.component';

describe('MonsterBattleCardPropComponent', () => {
  let component: MonsterBattleCardPropComponent;
  let fixture: ComponentFixture<MonsterBattleCardPropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonsterBattleCardPropComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MonsterBattleCardPropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
