import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-monster-battle-card-prop',
  templateUrl: './monster-battle-card-prop.component.html',
  styleUrls: ['./monster-battle-card-prop.component.scss']
})
export class MonsterBattleCardPropComponent {

  @Input() title = '';
  @Input() percentage = 0;

}
